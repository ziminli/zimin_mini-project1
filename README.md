# Mini-project 1

A static site generated using zola which holds all of the portfolio work in this class. The website uses the Zola adidoks template. 

➡️See the website **[here](https://zimin-mini-project1-ziminli-bc04a528199b18ce0427193295fd4b00abc.gitlab.io)**

## Home page
![Homepage](static/homepage.jpg)
