+++
title = "Portfolio"


# The homepage contents
[extra]
lead = '<br>Hi👋, I am Zimin Li. This site contains my works from IDS-721.<br><br>'
url = "/docs/general-information/general/"
url_button = "Get to Know Me"
repo_version = "GitHub v0.1.0"
repo_license = "Open-source MIT License."
repo_url = "https://github.com/ziminli"

# Menu items

[[extra.menu.main]]
name = "Resume"
section = "docs"
url = "/docs/general-information/general/"
weight = 10

[[extra.menu.main]]
name = "Projects"
section = "projects"
url = "/projects/"
weight = 20

[[extra.list]]
title = ""
content = ''

+++
