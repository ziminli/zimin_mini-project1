+++
title = "Education"
description = "Education experience."
date = 2021-05-01T18:10:00+00:00
updated = 2021-05-01T18:10:00+00:00
draft = false
weight = 410
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = '"Education is not preparation for life; education is life itself"  -- John Dewey'
toc = true
top = false
+++

## Master's
<b><u>University</u>: </b>Duke University

<b><u>Degree</u>: </b> Master of Science

<b><u>Major</u>: </b> Computer Engineering
