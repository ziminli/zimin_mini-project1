+++
title = "Extracurricular"
description = "Extracurricular experiences"
date = 2021-05-01T18:20:00+00:00
updated = 2021-05-01T18:20:00+00:00
draft = false
weight = 820
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = '"Experience is the teacher of all things."  -- Julius Caesar'
toc = true
top = false
+++
