+++
title = "Technical Skills"
description = "Technical skills"
date = 2021-05-01T18:20:00+00:00
updated = 2021-05-01T18:20:00+00:00
draft = false
weight = 420
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = '"Programming is a skill best acquired by practice and example rather than from books."  -- Alan Turing'
toc = true
top = false
+++

## Programming
C, C++, Python, Java, SQL, HTML

➡️check out the projects that I have done: [Projects](../../../projects/).