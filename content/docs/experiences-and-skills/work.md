+++
title = "Professional Experiences"
description = "Professional experiences"
date = 2021-05-01T18:20:00+00:00
updated = 2021-05-01T18:20:00+00:00
draft = false
weight = 420
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = '"Experience is the teacher of all things."  -- Julius Caesar'
toc = true
top = false
+++

## Teaching Assistant
September, 2023 - present

## Cloud Software Intern
May, 2023 - September, 2023

## Teaching Assistant
September, 2021 - May, 2022
