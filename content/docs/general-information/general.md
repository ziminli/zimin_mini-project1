+++
title = "General"
description = "General information"
date = 2021-05-01T08:00:00+00:00
updated = 2021-05-01T08:00:00+00:00
draft = false
weight = 10
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = 'General information about me'
toc = true
top = false
+++

## Biographical

<b>Name:</b> Zimin Li<br><b>Gender: </b>Male<br><b>Email: </b>zimin.li@duke.edu

## Hobbies

🗺️Travel &emsp;&emsp;&emsp;&emsp;&emsp;📷Photography

🏓Table tennis &emsp;&emsp;&ensp;🏸Badminton

🏊‍♂️Swimming &emsp;&emsp;&emsp;&nbsp;📚Reading

🎮Video games

... and more!