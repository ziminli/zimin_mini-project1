+++
title = "ILEscape: a 2D Escape Game"
description = "ILEscape: an escape game"
date = 2021-12-01T09:19:42+00:00
updated = 2021-12-01T09:19:42+00:00
draft = false
template = "blog/page.html"

[taxonomies]
authors = ["Zimin Li"]

[extra]
lead = "ILEScape is a 2D escape game developed in C++ with SFML library."
+++

2021.09 - 2021.12


- Served as the primary software architect and chief developer.
- Designed the software model of the game and developed the main structure, GUI, essential classes, animations, and multiple scenes and features,
including home page and backstory page, inventory, dialogue boxes, and some cipher puzzles.
- Used UML diagrams ad subsystem decomposition to create and adjust the design.
- Followed Agile principles, SCRUM, and used Git, JIRA, Confluence, and Bitbucket for managing the project.
- Performed black-box and white-box testing for all units and carried out integration testing in a bottom-up approach by writing test cases and test drivers.
- Formatted, reviewed, and wrote the functional and non-functional requirements of the Requirement Analysis Report (RAD) and various sections of the
System Design Document (SDD), such as class diagram and subsystem services. 

<img src="../../ILEscape.jpg" alt="ILEscape homepage" width="100%">

